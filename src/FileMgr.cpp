#include <unistd.h>
#include "FileMgr.h"

/**
 * Change the working directory.
 * @param path Where to change the working directory to.
 * @return On success 0. On error -1 is returned.
 */
int32 CFileMgr::ChangeDir(const char *path) {
    return chdir(path);
}