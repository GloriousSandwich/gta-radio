#pragma once

#include <cstdio>
#include "Common.h"

typedef FILE *FILESTREAM;

class CFileMgr {
public:
    static int32 ChangeDir(const char *path);
};