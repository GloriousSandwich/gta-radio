#pragma once

#include <memory>
#include <SFML/Audio.hpp>

#include "Common.h"

class CTrackLoader {
public:
    int32 m_nActiveTrackID;
    int8 m_nActiveTrackType;

public:
    CTrackLoader() = default;

    ~CTrackLoader() = default;

    sf::Music *LoadActiveTrack();

private:
    std::string GetActiveTrackName();
};

class CStreamingChannel {
public:
    sf::Music *m_pSoundStream;
    std::unique_ptr<CTrackLoader> m_pTrackLoader;
    uint32 m_nStreamPlayTimeMs;
    int32 m_nActiveTrackID;
    int32 m_nPlayingTrackID;

public:
    CStreamingChannel();
    ~CStreamingChannel() = default;

    void Initialize();
    bool IsSoundPlaying();
    sf::Time GetDuration();
    sf::Time GetPlayingOffset();

    void PlayTrack(int32 trackID, int8 trackType);
    void Play();
    void Pause();
    void Stop();

    int32 GetActiveTrackID();
    int32 GetPlayingTrackID();
    void LoadStream();

};



