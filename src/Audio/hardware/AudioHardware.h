#pragma once

#include <memory>

#include "Common.h"
#include "StreamingChannel.h"


class CAudioHardware {
public:
    bool m_bInitialized;

private:
    std::shared_ptr<CStreamingChannel> m_pStreamingChannel;

public:
    CAudioHardware() = default;
    ~CAudioHardware() = default;

    bool Initialize();
    bool IsSoundPlaying();
    sf::Time GetDuration();
    sf::Time GetPlayingOffset();

    void PlayTrack(int32 trackID, int8 trackType) const;
    void StartTrackPlayback() const;
    void PauseTrack();
    void StopTrack();
};

extern CAudioHardware AudioHardware;