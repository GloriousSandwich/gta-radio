#include <iostream>
#include <filesystem>

#include "StreamingChannel.h"
#include "RadioStreamsPC.h"
#include "RadioTrackManager.h"

/// Returns a pointer to the SoundStream;
sf::Music *CTrackLoader::LoadActiveTrack() {
    std::string trackName = GetActiveTrackName();
    std::string trackFileName = trackName + ".wav";

    std::cout << "Loading " << trackFileName << '\n';

    static sf::Music s_music;
    if (!s_music.openFromFile(trackFileName)) {
        std::cout << "Failed to load " << trackFileName << std::endl;
        exit(-1);
    }

    return &s_music;
}

std::string CTrackLoader::GetActiveTrackName() {
    switch (m_nActiveTrackType) {
        case TYPE_TRACK:
            return g_MusicTracksLookupTable[0].at(m_nActiveTrackID);
        case TYPE_INTRO:
            return g_IntroTracksLookupTable[0].at(m_nActiveTrackID);
        case TYPE_INDENT:
            return g_IndentTracksLookupTable[0].at(m_nActiveTrackID);
        case TYPE_DJ_BANTER:
            return g_DJBanterTracksLookupTable[0].at(m_nActiveTrackID);
        default:
            break;
    }
    return {""};
}

CStreamingChannel::CStreamingChannel() {
    m_pSoundStream = nullptr;
    m_pTrackLoader = std::make_unique<CTrackLoader>();
    m_nStreamPlayTimeMs = 0;
    m_nActiveTrackID = 0;
    m_nPlayingTrackID = 0;
}

bool CStreamingChannel::IsSoundPlaying() {
    return m_pSoundStream->getStatus() == sf::Music::Playing;
}


sf::Time CStreamingChannel::GetDuration() {
    return m_pSoundStream->getDuration();
}


sf::Time CStreamingChannel::GetPlayingOffset() {
    return m_pSoundStream->getPlayingOffset();
}

void CStreamingChannel::PlayTrack(int32 trackID, int8 trackType) {
    m_nActiveTrackID = trackID;
    m_pTrackLoader->m_nActiveTrackID = trackID;
    m_pTrackLoader->m_nActiveTrackType = trackType;
}

void CStreamingChannel::Play() {
    m_nPlayingTrackID = m_nActiveTrackID;
    m_pSoundStream->play();
}

void CStreamingChannel::Pause() {
    m_pSoundStream->pause();
}

void CStreamingChannel::Stop() {
    m_pSoundStream->stop();
}

int32 CStreamingChannel::GetActiveTrackID() {
    return m_nActiveTrackID;
}

int32 CStreamingChannel::GetPlayingTrackID() {
    return m_nPlayingTrackID;
}

void CStreamingChannel::LoadStream() {
    m_pSoundStream = m_pTrackLoader->LoadActiveTrack();
}
