#include "AudioHardware.h"
#include "StreamingChannel.h"

CAudioHardware AudioHardware = *new CAudioHardware;

/**
 * Initialize CAudioHardware.
 * @return True if initialized successfully.
 */
bool CAudioHardware::Initialize() {
    m_bInitialized = true;
    m_pStreamingChannel = std::make_shared<CStreamingChannel>();
    return true;
}

/**
 * @return True if sound is playing, false otherwise.
 */
bool CAudioHardware::IsSoundPlaying() {
    return m_pStreamingChannel->IsSoundPlaying();
}

/**
 * Get the total duration of the music.
 * @return Music duration
 */
sf::Time CAudioHardware::GetDuration() {
    return m_pStreamingChannel->GetDuration();
}

/**
 * Get the current playing position of the stream.
 * @return Current playing position from the beginning of the stream.
 */
sf::Time CAudioHardware::GetPlayingOffset() {
    return m_pStreamingChannel->GetPlayingOffset();
}

/**
 * Choose which track ID to play next.
 * @param trackID The ID of the track to play.
 * @param trackType The type of track to play.
 */
void CAudioHardware::PlayTrack(int32 trackID, int8 trackType) const {
    m_pStreamingChannel->PlayTrack(trackID, trackType);
}

/**
 * This function starts the stream if it was stopped, resumes it if it was paused, and restarts it from the beginning
 * if it was already playing.
 */
void CAudioHardware::StartTrackPlayback() const {
    m_pStreamingChannel->LoadStream();
    m_pStreamingChannel->Play();
}

/**
 * This function pauses the stream if it was playing, otherwise (stream already paused or stopped) it has no effect.
 */
void CAudioHardware::PauseTrack() {
    m_pStreamingChannel->Pause();
}

/**
 * This function stops the stream if it was playing or paused, and does nothing if it was already stopped. It also resets
 * the playing position (unlike pause()).
 */
void CAudioHardware::StopTrack() {
    m_pStreamingChannel->Stop();
}
