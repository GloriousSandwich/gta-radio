#pragma once

#include <cstdlib>
#include "Common.h"

class CAudioUtility {
public:
    static bool ResolveProbability(float prob);
    static int32 GetRandomNumber();
    static int32 GetRandomNumberInRange(int32 min, int32 max);
};