#include <string>
#include <unordered_map>
#include <array>
#include "Common.h"

static const std::unordered_map<int32, std::string> g_MusicTracksLookupTable[] = {
        { // The Lowdown 91.1.
                {1, "ASHLEYS_ROACHCLIP"},
                {6, "BOUNCY_LADY"},
                {11, "CALIFORNIA_SOUL"},
                {16, "CHANGIN"},
                {21, "CLIMAX"},
                {26, "CRUISIN"},
                {31, "DO_IT_TIL_YOURE_SATISFIED"},
                {36, "FUNNY_FEELING"},
                {41, "HERCULES"},
                {46, "I_BELIEVE_IN_MIRACLES"},
                {51, "I_GET_LIFTED"},
                {56, "MAGIC_MOUNTAIN"},
                {61, "O_O_H_CHILD"},
                {65, "READY_OR_NOT"},
                {68, "RUBBER_BAND"},
                {71, "SMILING_FACES"},
                {74, "STORIES"},
                {77, "SUPERMAN_LOVER"},
                {80, "THE_CISCO_KID"},
                {83, "VIVA_TIRADO"},
        }
};

static const std::unordered_map<int32, std::string> g_IntroTracksLookupTable[] = {
        { // The Lowdown 91.1.
                {2, "ASHLEYS_ROACHCLIP_01"},
                {7, "ASHLEYS_ROACHCLIP_02"},
                {12, "CALIFORNIA_SOUL_01"},
                {17, "CALIFORNIA_SOUL_02"},
                {22, "CLIMAX_01"},
                {27, "CLIMAX_02"},
                {32, "CRUISIN_01"},
                {37, "CRUISIN_02"},
                {42, "DO_IT_TIL_YOURE_SATISFIED_01"},
                {47, "DO_IT_TIL_YOURE_SATISFIED_02"},
                {52, "FUNNY_FEELING_01"},
                {57, "I_BELIEVE_IN_MIRACLES_01"},
                {62, "I_BELIEVE_IN_MIRACLES_02"},
                {66, "I_GET_LIFTED_01"},
                {69, "I_GET_LIFTED_02"},
                {72, "MAGIC_MOUNTAIN_01"},
                {75, "MAGIC_MOUNTAIN_02"},
                {78, "O_O_H_CHILD_01"},
                {81, "O_O_H_CHILD_02"},
                {84, "READY_OR_NOT_01"},
                {86, "READY_OR_NOT_02"},
                {88, "RUBBER_BAND_01"},
                {90, "RUBBER_BAND_02"},
                {92, "SMILING_FACES_01"},
                {94, "SMILING_FACES_02"},
                {96, "SUPERMAN_LOVER_01"},
                {98, "SUPERMAN_LOVER_02"},
                {100, "THE_CISCO_KID_01"},
                {102, "THE_CISCO_KID_02"},
                {104, "VIVA_TIRADO_01"},
                {106, "VIVA_TIRADO_01"},
        }
};

static const std::unordered_map<int32, std::string> g_GeneralTracksLookupTable[] = {
        { // The Lowdown 91.1.
                {3, "GENERAL_01"},
                {8, "GENERAL_02"},
                {13, "GENERAL_03"},
                {18, "GENERAL_04"},
                {23, "GENERAL_05"},
                {28, "GENERAL_06"},
                {33, "GENERAL_07"},
                {38, "GENERAL_08"},
                {43, "GENERAL_09"},
                {48, "GENERAL_10"},
                {53, "GENERAL_11"},
                {58, "GENERAL_12"},
                {63, "GENERAL_13"},
                {67, "GENERAL_14"},
                {70, "GENERAL_15"},
                {73, "GENERAL_16"},
                {76, "GENERAL_17"},
                {79, "GENERAL_18"},
                {82, "GENERAL_19"},
                {85, "GENERAL_20"},
                {87, "GENERAL_21"},
                {89, "GENERAL_22"},
                {91, "GENERAL_23"},
                {93, "GENERAL_25"},
                {95, "GENERAL_26"},
                {97, "GENERAL_27"},
                {99, "GENERAL_28"},
                {101, "GENERAL_29"},
                {103, "GENERAL_30"},
                {105, "GENERAL_31"},
                {107, "GENERAL_32"},
                {108, "GENERAL_33"},
                {109, "GENERAL_34"},
                {110, "GENERAL_35"},
                {111, "GENERAL_36"},
        }
};

static const std::unordered_map<int32, std::string> g_IndentTracksLookupTable[] = {
        { // The Lowdown 91.1.
                {4, "ID_01"},
                {9, "ID_02"},
                {14, "ID_03"},
                {19, "ID_04"},
                {24, "ID_05"},
                {29, "ID_06"},
                {34, "ID_07"},
                {39, "ID_08"},
                {44, "ID_09"},
                {49, "ID_10"},
                {54, "ID_11"},
                {59, "ID_12"},
                {64, "ID_13"},
        }
};

static const std::unordered_map<int32, std::string> g_DJBanterTracksLookupTable[] = {
        { // The Lowdown 91.1.
                {5, "MONO_SOLO_01"},
                {10, "MONO_SOLO_02"},
                {15, "MONO_SOLO_03"},
                {20, "MONO_SOLO_04"},
                {25, "MONO_SOLO_05"},
                {30, "MONO_SOLO_06"},
                {35, "MONO_SOLO_07"},
                {40, "MONO_SOLO_08"},
                {45, "MONO_SOLO_09"},
                {50, "MONO_SOLO_10"},
                {55, "MONO_SOLO_11"},
                {60, "MONO_SOLO_12"},

        }
};

/// 2D array listing which music track ID belongs to which radio station.
static const std::array<std::array<int32, 20>, 1> g_RadioMusicTracks = {
        {
                1, 6, 11, 16, 21, 26, 31, 36, 41, 46, 51, 56, 61, 65, 68, 71, 74, 77, 80, 83,
        }
};

/**
 * 3D array listing which pair of intro tracks belongs to which music track for
 * which radio station.\n\n
 *
 * Format: {Music track id, 1st intro track id, 2nd intro track id}
 */
static const std::array<std::array<std::array<int32, 3>, 16>, 1> g_RadioIntroTracks = {{
    {{ // The Lowdown 91.1
        {1, 2, 7},
        {11, 12, 17},
        {21, 22, 27},
        {26, 32, 37},
        {31, 42, 47},
        {36, 52, 52},
        {46, 57, 62},
        {51, 66, 69},
        {56, 72, 75},
        {61, 78, 81},
        {65, 84, 86},
        {68, 88, 90},
        {71, 92, 94},
        {77, 96, 98},
        {80, 100, 102},
        {83, 104, 106},
        }}
}};

static const std::array<std::array<int32, 35>, 1> g_RadioGeneralTracks = {
        {
                3, 8, 13, 18, 23, 28, 33, 38, 43, 48, 53, 58, 63, 67, 70, 73, 76, 79, 82, 85, 87, 89,
                91, 93, 95, 97, 99, 101, 103, 105, 107, 108, 109, 110, 111,
        }
};

static const std::array<std::array<int32, 12>, 1> g_RadioIndentTracks = {
        {
                4, 9, 14, 19, 24, 29, 34, 44, 49, 54, 49, 64
        }
};

static const std::array<std::array<int32, 12>, 1> g_RadioDJBanterTracks = {
        {
                5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60
        }
};