#include "AudioUtility.h"
#include <random>

bool CAudioUtility::ResolveProbability(float prob) {
    return prob >= 1.0F || ((float) GetRandomNumber() * RAND_MAX_FLOAT_RECIPROCAL) < prob;
}

/**
 * @return A pseudo-random number between 0 and RAND_MAX.
 */
int32 CAudioUtility::GetRandomNumber() {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int32> dist(0, RAND_MAX);
    return dist(mt);
}

/**
 * Returns a pseudo-random number between min and max, exclusive [min, max).
 * @param  min Minimum value
 * @param  max Maximum value. Must be greater than min.
 * @return Integer between min and max, exclusive.
 */
int32 CAudioUtility::GetRandomNumberInRange(int32 min, int32 max) {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int32> dist(min, max);
    return dist(mt);
}

