#pragma once

#include "eRadioStationID.h"

class CAudioEngine {
public:
    eRadioStationID m_nCurrentRadioStationID;
    eRadioStationID m_nSavedRadioStationID;

public:
    CAudioEngine() = default;

    ~CAudioEngine() = default;

    bool Initialize();

    void StartRadio(eRadioStationID id);
};

extern CAudioEngine AudioEngine;