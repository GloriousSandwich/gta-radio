#pragma once

#include <cstdlib>
#include <cstdint>
#include <array>

#include "Common.h"
#include "eRadioStationID.h"

enum nt : int8 {
    TYPE_INDENT = 0,
    TYPE_ADVERT = 1,
    TYPE_DJ_BANTER = 2,
    TYPE_INTRO = 3,
    TYPE_TRACK = 4,
    TYPE_OUTRO = 5,
    TYPE_NONE = 6,
    TYPE_USER_TRACK = 7,
};

struct tRadioSettings {
    std::array<int32, 5> m_aTrackQueue;
    std::array<int32, 5> m_aTrackIDs;
    int32 m_iCurrentTrackID;
    int32 m_iPrevTrackID;
    int32 m_iTrackPlayTime;
    int32 m_iTrackLengthMs;
    int8 m_nCurrentRadioStation;
    int8 m_nBassSet;
    float m_fBassGain;
    std::array<int8, 5> m_aTrackTypes;
    int8 m_iCurrentTrackType;
    int8 m_iPrevTrackType;

    void Reset() {
        for (auto i = 0u; i < m_aTrackQueue.size(); i++) {
            m_aTrackQueue[i] = -1;
            m_aTrackTypes[i] = TYPE_NONE;
            m_aTrackIDs[i] = -1;
        }
    }
};

class CRadioTrackManager {
public:
    bool m_bInitialised;
    bool m_bDisplayStationName;
    tRadioSettings m_settings;

public:
    static std::array<std::vector<int32>, 1> m_nMusicTrackHistory;
    static std::array<std::vector<int32>, 1> m_nDJBanterHistory;
    static std::array<std::vector<int32>, 1> m_nIndentHistory;

public:
    CRadioTrackManager() = default;
    ~CRadioTrackManager() = default;

    bool Initialize();
    [[noreturn]] void StartRadio(eRadioStationID id);
    int32 QueueUpTracksForStation(eRadioStationID id, uint8 *iTrackCount, int8_t trackType, tRadioSettings *settings) const;

protected:
    void ChooseTracksForStation(eRadioStationID id);
    static int32 ChooseMusicTrackID(eRadioStationID id);
    static int32 ChooseIntroTrackID(eRadioStationID id);
    static int32 ChoseIndentTrackID(eRadioStationID id);
    static int32 ChooseDJBanterTrackID(eRadioStationID id);
    static bool AddMusicTrackIDToHistory(eRadioStationID id, int32 trackID);
    static bool AddDJBanterIDToHistory(eRadioStationID id, int32 trackID);
    static bool AddIndentIDToHistory(eRadioStationID id, int32 trackID);
};

extern CRadioTrackManager RadioTrackManager;