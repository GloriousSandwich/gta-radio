#include <iostream>
#include <thread>

#include "RadioTrackManager.h"
#include "RadioStreamsPC.h"
#include "FileMgr.h"
#include "AudioUtility.h"
#include "AudioHardware.h"

CRadioTrackManager RadioTrackManager = *new CRadioTrackManager;

std::array<std::vector<int32>, 1> CRadioTrackManager::m_nMusicTrackHistory;
std::array<std::vector<int32>, 1> CRadioTrackManager::m_nDJBanterHistory;
std::array<std::vector<int32>, 1> CRadioTrackManager::m_nIndentHistory;

bool CRadioTrackManager::Initialize() {
    m_bInitialised = true;
    m_bDisplayStationName = false;
    return true;
}

/**
 * Starts the radio with the radio station to play.
 * @param id The radio station ID to play.
 */
[[noreturn]] void CRadioTrackManager::StartRadio(eRadioStationID id) {
    m_settings.m_nCurrentRadioStation = id;
    const int32 code = CFileMgr::ChangeDir("/home/elmo/tmp/Audio V2 Testing/RADIO_15");
    if (code != 0) {
        std::cout << "Failed to change directory" << std::endl;
        exit(code);
    }

    while (true) {
        ChooseTracksForStation(id);

        for (int32 i: m_settings.m_aTrackQueue) {
            if (i == -1)
                continue;

            const int32 trackID = m_settings.m_aTrackIDs.at(i);

            if (trackID == -1)
                continue;

            const int8 trackType = m_settings.m_aTrackTypes.at(i);

            AudioHardware.PlayTrack(trackID, trackType);
            AudioHardware.StartTrackPlayback();

            float durationSeconds = AudioHardware.GetDuration().asSeconds();
            while (AudioHardware.IsSoundPlaying()) {
                float playingOffsetSeconds = AudioHardware.GetPlayingOffset().asSeconds();
                std::cout << "\r" << "Playing Track: " << playingOffsetSeconds << "/" << durationSeconds << " seconds"
                          << std::flush;
                std::this_thread::sleep_for(std::chrono::seconds(1));
            }
            std::cout << '\n';
        }
    }
}

void CRadioTrackManager::ChooseTracksForStation(eRadioStationID id) {
    std::cout << "Choosing tracks for radio station\n";
    uint8 trackCount = 0;
    tRadioSettings *settings = &m_settings;

    settings->Reset();

    if (!CAudioUtility::ResolveProbability(0.95F)) {
        if (CAudioUtility::ResolveProbability(0.5F)) {
            QueueUpTracksForStation(id, &trackCount, TYPE_INDENT, settings);
        }

        QueueUpTracksForStation(id, &trackCount, TYPE_DJ_BANTER, settings);
        QueueUpTracksForStation(id, &trackCount, TYPE_INTRO, settings);
        return;
    }

    if (CAudioUtility::ResolveProbability(0.9F)) {
        QueueUpTracksForStation(id, &trackCount, TYPE_TRACK, settings);
        return;
    }

    if (CAudioUtility::ResolveProbability(0.5F)) {
        if (CAudioUtility::ResolveProbability(0.5)) {
            QueueUpTracksForStation(id, &trackCount, TYPE_INDENT, settings);
        }

        QueueUpTracksForStation(id, &trackCount, TYPE_INTRO, settings);
        return;
    }

    if (CAudioUtility::ResolveProbability(0.5F)) {
        QueueUpTracksForStation(id, &trackCount, TYPE_INDENT, settings);
    }
}

int32 CRadioTrackManager::QueueUpTracksForStation(eRadioStationID id, uint8 *iTrackCount, int8 trackType,
                                                  tRadioSettings *settings) const {
    if (*iTrackCount > m_settings.m_aTrackQueue.size())
        return -1;

    int32 trackID = -1;
    bool success = false;
    switch (trackType) {
        case TYPE_TRACK :
            while (!success) {
                trackID = ChooseMusicTrackID(id);
                success = AddMusicTrackIDToHistory(id, trackID);
            }
            break;
        case TYPE_INTRO:
            trackID = ChooseIntroTrackID(id);
            break;
        case TYPE_INDENT:
            while (!success) {
                trackID = ChoseIndentTrackID(id);
                success = AddIndentIDToHistory(id, trackID);
            }
            break;
        case TYPE_DJ_BANTER:
            while (!success) {
                trackID = ChooseDJBanterTrackID(id);
                success = AddDJBanterIDToHistory(id, trackID);
            }
        default:
            break;
    };

    settings->m_aTrackQueue.at(*iTrackCount) = *iTrackCount;
    settings->m_aTrackIDs.at(*iTrackCount) = trackID;
    settings->m_aTrackTypes.at(*iTrackCount) = trackType;
    *iTrackCount += 1;

    //  Find the corresponding music track for the intro track.
    if (trackType == TYPE_INTRO) {
        for (const auto &introTracks: g_RadioIntroTracks[0]) {
            for (const int32 innerTrackID: introTracks) {
                if (innerTrackID == trackID) {
                    const int32 musicTrackID = introTracks.at(0);
                    settings->m_aTrackQueue.at(*iTrackCount) = *iTrackCount;
                    settings->m_aTrackIDs.at(*iTrackCount) = musicTrackID;
                    settings->m_aTrackTypes.at(*iTrackCount) = TYPE_TRACK;
                    *iTrackCount += 1;
                    goto break_outer_loop;
                }
            }
        }
        break_outer_loop:;
    }

    return 0;
}

int32 CRadioTrackManager::ChooseMusicTrackID(eRadioStationID id) {
    if (id == RADIO_MOTOWN) {
        const auto arraySize = static_cast<int32>(sizeof(g_RadioMusicTracks[0]) / sizeof(g_RadioMusicTracks[0][0]));
        const int32 index = CAudioUtility::GetRandomNumberInRange(0, arraySize - 1);
        const int32 trackID = g_RadioMusicTracks[0][index];
        return trackID;
    }
    return -1;
}

int32 CRadioTrackManager::ChooseIntroTrackID(eRadioStationID id) {
    if (id == RADIO_MOTOWN) {
        std::array<std::array<int32, 3>, 16> outerIntroTracks = g_RadioIntroTracks[0];
        const auto outerArraySize = static_cast<int32>(sizeof(g_RadioIntroTracks[0]) /
                                                       sizeof(g_RadioIntroTracks[0][0]));
        const int32 outerIndex = CAudioUtility::GetRandomNumberInRange(0, outerArraySize - 1);
        const std::array<int32, 3> innerIntroTracks = outerIntroTracks.at(outerIndex);
        const int32 innerIndex = CAudioUtility::GetRandomNumberInRange(1, 2);
        const int32 trackID = innerIntroTracks.at(innerIndex);
        return trackID;
    }
    return -1;
}

int32 CRadioTrackManager::ChoseIndentTrackID(eRadioStationID id) {
    if (id == RADIO_MOTOWN) {
        const auto arraySize = static_cast<int32>(sizeof(g_RadioIndentTracks[0]) / sizeof(g_RadioIndentTracks[0][0]));
        const int32 index = CAudioUtility::GetRandomNumberInRange(0, arraySize - 1);
        const int32 trackID = g_RadioIndentTracks[0][index];
        return trackID;
    }
    return -1;
}

int32 CRadioTrackManager::ChooseDJBanterTrackID(eRadioStationID id) {
    if (id == RADIO_MOTOWN) {
        const auto arraySize = static_cast<int32>(sizeof(g_RadioDJBanterTracks[0]) /
                                                  sizeof(g_RadioDJBanterTracks[0][0]));
        const int32 index = CAudioUtility::GetRandomNumberInRange(0, arraySize - 1);
        const int32 trackID = g_RadioDJBanterTracks[0][index];
        return trackID;
    }
    return -1;
}


/**
 * @param id Radio station ID.
 * @param trackID ID of the track to be played.
 * @return True if the track ID has been added to the history. False if the track ID already exists in the history.
 */
bool CRadioTrackManager::AddMusicTrackIDToHistory(eRadioStationID id, int32 trackID) {
    if (id == RADIO_MOTOWN) {
        if (m_nMusicTrackHistory[0].size() >= 19)
            m_nMusicTrackHistory[0].clear();

        bool containsTrackID = false;
        for (const auto i: m_nMusicTrackHistory[0]) {
            if (i == trackID) {
                containsTrackID = true;
            }
        }

        if (!containsTrackID) {
            m_nMusicTrackHistory[0].push_back(trackID);
            return true;
        }
    }
    return false;
}

/**
 * @param id Radio station ID.
 * @param trackID ID of the track to be played.
 * @return True if the track ID has been added to the history. False if the track ID already exists in the history.
 */
bool CRadioTrackManager::AddDJBanterIDToHistory(eRadioStationID id, int32 trackID) {
    if (id == RADIO_MOTOWN) {
        if (m_nDJBanterHistory[0].size() >= 15)
            m_nDJBanterHistory[0].clear();

        bool containsTrackID = false;
        for (const auto i: m_nDJBanterHistory[0]) {
            if (i == trackID) {
                containsTrackID = true;
            }
        }

        if (!containsTrackID) {
            m_nDJBanterHistory[0].push_back(trackID);
            return true;
        }
    }
    return false;
}

/**
 * @param id Radio station ID.
 * @param trackID ID of the track to be played.
 * @return True if the track ID has been added to the history. False if the track ID already exists in the history.
 */
bool CRadioTrackManager::AddIndentIDToHistory(eRadioStationID id, int32 trackID) {
    if (id == RADIO_MOTOWN) {
        if (m_nIndentHistory[0].size() >= 8)
            m_nIndentHistory[0].clear();

        bool containsTrackID = false;
        for (const auto i: m_nIndentHistory[0]) {
            if (i == trackID) {
                containsTrackID = true;
            }
        }

        if (!containsTrackID) {
            m_nIndentHistory[0].push_back(trackID);
            return true;
        }
    }
    return false;
}
