#include <iostream>

#include "AudioEngine.h"
#include "AudioHardware.h"
#include "RadioTrackManager.h"

CAudioEngine AudioEngine = *new CAudioEngine;

/**
 * Initialize the Audio Engine and global variables.
 * @return True if successfully initialized. False if it failed to initialize a global variable.
 */
bool CAudioEngine::Initialize() {
    if (!AudioHardware.Initialize()) {
        std::cout << "[AudioEngine] Failed to initialize Audio Hardware\n";
        return false;
    }

    if (!RadioTrackManager.Initialize()) {
        std::cout << "[AudioEngine] Failed to initialize Radio Track Manager";
        return false;
    }
    return true;
}

/**
 * Starts the radio with the radio station ID to play.
 * @param id The radio station ID to play.
 */
void CAudioEngine::StartRadio(eRadioStationID id) {
    RadioTrackManager.StartRadio(id);
}
