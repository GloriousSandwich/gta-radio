#include <iostream>
#include "eRadioStationID.h"
#include "AudioEngine.h"
#include "AudioHardware.h"
#include "FileMgr.h"

int main() {
    if (!AudioEngine.Initialize())
        return -1;

    AudioEngine.StartRadio(RADIO_MOTOWN);
    return 0;
}
